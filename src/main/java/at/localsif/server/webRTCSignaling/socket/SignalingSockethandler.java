package at.localsif.server.webRTCSignaling.socket;

import at.localsif.server.webRTCSignaling.model.SignalMessage;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;

import java.util.HashMap;
import java.util.Map;

public class SignalingSockethandler extends TextWebSocketHandler {

    private static final Logger LOG = LoggerFactory.getLogger(SignalingSockethandler.class);

    private static final String LOGIN_TYPE = "login";
    private static final String RTC_TYPE = "rtc";


    // Jackson JSON converter
    private ObjectMapper objectMapper = new ObjectMapper();

    // Here is our Directory (MVP way)
    // This map saves sockets by ClientCode
    private Map<String, WebSocketSession> clients = new HashMap<String, WebSocketSession>();
    // Thus map saves ClientCode by socket ID
    private Map<String, String> clientIds = new HashMap<String, String>();

    @Override
    protected void handleTextMessage(WebSocketSession session, TextMessage message) throws Exception {


        SignalMessage signalMessage = objectMapper.readValue(message.getPayload(), SignalMessage.class);

        //region Logging
        LOG.info("-----------------------------------------Message IN-------------------------------------------------");
        LOG.info("Type:        "+signalMessage.getType());
        LOG.info("Destination: "+signalMessage.getDest());
        LOG.info("Data:        "+signalMessage.getData());
        LOG.info("SDP:         \n"+signalMessage.getSdp());
        LOG.info("ICE:         "+signalMessage.getIce());
        LOG.info("----------------------------------------------------------------------------------------------------");
        //endregion

        //region LOGIN
        if (LOGIN_TYPE.equalsIgnoreCase(signalMessage.getType())) {
            // It's a login message so we assume data to be a String representing the clientCode
            String clientCode = (String) signalMessage.getData();

            WebSocketSession client = clients.get(clientCode);


            // quick check to verify that the clientCode is not already taken and active
            if (client == null || !client.isOpen()) {
                // saves socket and clientCode
                clients.put(clientCode, session);
                clientIds.put(session.getId(), clientCode);

                client = clients.get(clientCode);

                //Create Response Message
                SignalMessage acceptMsg= new SignalMessage();
                acceptMsg.setDest(clientCode);
                acceptMsg.setType("Server");
                acceptMsg.setData("ACCEPTED");

                //region Logging
                LOG.info("-------------------------------------Login Message OUT----------------------------------------------");
                LOG.info("Type:        "+acceptMsg.getType());
                LOG.info("Destination: "+acceptMsg.getDest());
                LOG.info("Data:        "+acceptMsg.getData());
                LOG.info("SDP:         "+acceptMsg.getSdp());
                LOG.info("ICE:         "+acceptMsg.getIce());
                LOG.info("----------------------------------------------------------------------------------------------------");
                //endregion

                // Convert our object back to JSON
                String acceptMsgString = objectMapper.writeValueAsString(acceptMsg);
                //send Message
                client.sendMessage(new TextMessage(acceptMsgString));

            } else { SignalMessage acceptMsg= new SignalMessage();
                //Create Response Message
                acceptMsg.setDest(clientCode);
                acceptMsg.setType("Server");
                acceptMsg.setData("NOT ACCEPTED");

                //region Logging
                LOG.info("-------------------------------------Login Message OUT----------------------------------------------");
                LOG.info("Type:        "+acceptMsg.getType());
                LOG.info("Destination: "+acceptMsg.getDest());
                LOG.info("Data:        "+acceptMsg.getData());
                LOG.info("SDP:         "+acceptMsg.getSdp());
                LOG.info("ICE:         "+acceptMsg.getIce());
                LOG.info("----------------------------------------------------------------------------------------------------");
                //endregion

                // Convert our object back to JSON
                String acceptMsgString = objectMapper.writeValueAsString(acceptMsg);
                //send Message
                client.sendMessage(new TextMessage(acceptMsgString));

            }

        }
        //endregion
        //region Communication between clients
        else if (RTC_TYPE.equalsIgnoreCase(signalMessage.getType())) {

            // with the dest username, we can find the targeted socket, if any
            String dest = signalMessage.getDest();
            WebSocketSession destSocket = clients.get(dest);
            // if the socket exists and is open, we go on
            if (destSocket != null && destSocket.isOpen()) {

                // We write the message to send to the dest socket (it's our propriatary format)

                SignalMessage out = new SignalMessage();
                // still an RTC type
                out.setType(RTC_TYPE);
                // we use the dest field to specify the actual exp., but it will be the next dest.
                out.setDest(clientIds.get(session.getId()));

                //Create Response Message - Data does not get changed, just relayed to other CLient
                out.setData(signalMessage.getData());
                out.setIce(signalMessage.getIce());
                out.setSdp(signalMessage.getSdp());
                out.setDest(signalMessage.getDest());

                //region Logging
                LOG.info("-------------------------------------RTC Message OUT------------------------------------------------");
                LOG.info("Type:        "+out.getType());
                LOG.info("Destination: "+out.getDest());
                LOG.info("Data:        "+out.getData());
                LOG.info("SDP:         \n"+out.getSdp());
                LOG.info("ICE:         "+out.getIce());
                LOG.info("----------------------------------------------------------------------------------------------------");
                //endregion

                // Convert our object back to JSON
                String stringifiedJSONmsg = objectMapper.writeValueAsString(out);

                //send Message
                destSocket.sendMessage(new TextMessage(stringifiedJSONmsg));
            }
        }
        //endregion

    }
}