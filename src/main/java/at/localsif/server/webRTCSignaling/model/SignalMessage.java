package at.localsif.server.webRTCSignaling.model;

public class SignalMessage {

    private String type;
    private String dest;
    private Object data;
    private Object sdp;
    private Object ice;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDest() {
        return dest;
    }

    public void setDest(String dest) {
        this.dest = dest;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public Object getSdp() {
        return sdp;
    }

    public void setSdp(Object sdp) {
        this.sdp = sdp;
    }

    public Object getIce() {
        return ice;
    }

    public void setIce(Object ice) {
        this.ice = ice;
    }
}