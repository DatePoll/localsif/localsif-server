FROM openjdk:11.0.8-slim-buster
ADD target/server-0.0.1.jar localsif.jar
ENTRYPOINT ["java", "-jar", "localsif.jar"]

